DROP TABLE IF EXISTS container CASCADE;
DROP TABLE IF EXISTS container_run_options CASCADE;

CREATE TABLE container (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    image TEXT NOT NULL,
    environment TEXT NOT NULL
);

CREATE TABLE container_run_options (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    container_id BIGINT NOT NULL REFERENCES container(id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    "key" TEXT NOT NULL,
    value TEXT NOT NULL,
    active BOOL NOT NULL DEFAULT true
);