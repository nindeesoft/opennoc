-- ENVIRONMENT LOOKUP
DROP TABLE IF EXISTS lu_environments CASCADE;

CREATE TABLE lu_environments (
    id               SERIAL NOT NULL PRIMARY KEY,
    environment_full TEXT   NOT NULL UNIQUE,
    environment_abrv TEXT   NOT NULL UNIQUE,
    description      TEXT       NULL,
    active           BOOL   NOT NULL DEFAULT TRUE
);

INSERT INTO lu_environments (environment_full, environment_abrv, description, active)
VALUES
    ('Testing','TST','This environment is for testing.',TRUE),
    ('Staging','STG','This environment is used before UAT and Production.', TRUE),
    ('User Acceptance Testing','UAT','Allows users to test before production',TRUE),
    ('Production','PRD','Production',TRUE);
    
-- DEVICE TYPE
DROP TABLE IF EXISTS lu_device_types CASCADE;

CREATE TABLE lu_device_types (
    id          SERIAL NOT NULL PRIMARY KEY,
    device_type TEXT   NOT NULL UNIQUE,
    description TEXT       NULL,
    active      BOOL   NOT NULL DEFAULT TRUE
);

INSERT INTO lu_device_types (device_type)
VALUES
    ('Workstation'),
    ('Laptop'),
    ('Server'),
    ('IoT');