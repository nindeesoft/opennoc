DROP TABLE IF EXISTS computer CASCADE;
DROP TABLE IF EXISTS ssh CASCADE;
DROP TABLE IF EXISTS ip_address CASCADE;

CREATE TABLE computer (
    id               BIGSERIAL NOT NULL PRIMARY KEY,
    hostname         TEXT      NOT NULL,
    domain_name      TEXT          NULL,
    "type"           TEXT      NOT NULL,
    operating_system TEXT      NOT NULL,
    virtual          BOOL      NOT NULL DEFAULT FALSE,
    ssh_enabled      BOOL      NOT NULL DEFAULT FALSE,
    active           BOOL      NOT NULL DEFAULT TRUE
);

CREATE TABLE ssh (
    id         BIGSERIAL NOT NULL PRIMARY KEY,
    username   TEXT      NOT NULL UNIQUE,
    public_key TEXT      NOT NULL,
    active     BOOL      NOT NULL DEFAULT TRUE
);

CREATE TABLE ip_address (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    address INET NOT NULL UNIQUE
);